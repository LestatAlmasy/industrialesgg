package stacarolina;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class StaCarolina {

    public static void main(String[] args) throws IOException {
        
        //sc lee enteros ; scS lee string (texto)
        Scanner sc = new Scanner(System.in);
        Scanner scS = new Scanner(System.in);
        
        //opcion: variable que escoge dentro de switch (estructura de menu) principal
        //op1: variable que escoge dentro de switch (estructura de menu) ligada a cada cuenta
        //indicecuenta: almacena la posicion de la cuenta que estamos consultando
        //tipo: indica si estoy ingresando un cheque, vale vista o efectivo para cobro/pago
        int opcion, op1, indiceCuenta, tipo;
        
        //Lista de cuentas ligadas a la empresa
        ArrayList<Cuenta> rescatadas = new ArrayList<>();
        //Variable que sirve para ejecutar menu hasta que cambie a falso (en caso 4 de salir)
        boolean continuar= true;
        
        //Permite ingresados a los metodos de la viña
        StaCarolina s = new StaCarolina();
               
        while(continuar){
            
            System.out.println("");
            System.out.println("MENU");
            System.out.println("");
            System.out.println("1. Administrar cuentas existentes");
            System.out.println("2. Agregar cuenta");
            System.out.println("3. Salir");
            opcion = sc.nextInt();

            switch(opcion){
                case 1:
                    //ADMINISTRAR CUENTAS
                    
                    //Leo el archivo de texto "Cuentas.txt" y obtengo los datos de las cuentas
                    //que han sido almacenadas
                    rescatadas = s.rescatarCuentas();
                    System.out.println("¿Sobre qué cuenta desea operar?");
                    for(int i=0; i<rescatadas.size();i++){
                        System.out.println(i+". "+rescatadas.get(i).getNombre());
                    }
                    indiceCuenta = sc.nextInt();
                    System.out.println("1. Pago");
                    System.out.println("2. Cobro");
                    System.out.println("3. Historial (Ingresos y egresos)");
                    op1 = sc.nextInt();
                    switch(op1){
                        /*
                        Cada movimiento posee:
                        |Cliente cliente;
                        |String fecha;
                        |int monto;
                        |String motivo;
                        |Cuenta cuenta;
                        //cheque, vale vista, efectivo
                        |String tipo;
                        */
                        case 1:
                            //PAGO
                            
                            //Se crea el objeto m de la clase movimiento para utilizar
                            //el metodo pago una vez los datos hayan sido ingresados
                            Movimiento m = new Movimiento();
                            Cliente cl = new Cliente();
                            System.out.println("** Usted efectuará un pago **");
                            m.setCuenta(rescatadas.get(indiceCuenta));
                            System.out.println("Ingrese monto");
                            m.setMonto(sc.nextInt());
                            System.out.println("Ingrese motivo");
                            m.setMotivo(scS.nextLine());
                            System.out.println("Ingrese nombre de quien paga");
                            cl.setNombre(scS.nextLine());
                            System.out.println("Ingrese apellido de quien paga");
                            cl.setApellido(scS.nextLine());
                            m.setCliente(cl);
                            System.out.println("Ingrese fecha (dd/mm/aaaa)");
                            m.setFecha(scS.nextLine());
                            System.out.println("Ingrese tipo");
                            System.out.println("1. Cheque");
                            System.out.println("2. Vale vista");
                            System.out.println("3. Efectivo");
                            tipo = sc.nextInt();
                            if(tipo == 1){
                                m.setTipo("Cheque");
                            }else{
                                if(tipo ==  2){
                                    m.setTipo("Vale vista");
                                }else{
                                    if(tipo == 3){
                                        m.setTipo("Efectivo");
                                    }
                                }
                            }
                            
                            //Llamada metodo pago. Si el pago no funciona es porque surgio un error
                            if(!m.pago()){
                                System.out.println("");
                                System.out.println("Probablemente surgio un error");
                            }

                            break;
                            
                        case 2:
                            //COBRO
                            
                            Movimiento m1 = new Movimiento();
                            Cliente cl1 = new Cliente();
                            System.out.println("** Usted efectuará un cobro **");
                            m1.setCuenta(rescatadas.get(indiceCuenta));
                            System.out.println("Ingrese monto");
                            m1.setMonto(sc.nextInt());
                            System.out.println("Ingrese motivo");
                            m1.setMotivo(scS.nextLine());
                            System.out.println("Ingrese nombre de quien cobra");
                            cl1.setNombre(scS.nextLine());
                            System.out.println("Ingrese apellido de quien cobra");
                            cl1.setApellido(scS.nextLine());
                            m1.setCliente(cl1);
                            System.out.println("Ingrese fecha (dd/mm/aaaa)");
                            m1.setFecha(scS.nextLine());
                            System.out.println("Ingrese tipo");
                            System.out.println("1. Cheque");
                            System.out.println("2. Vale vista");
                            System.out.println("3. Efectivo");
                            tipo = sc.nextInt();
                            if(tipo == 1){
                                m1.setTipo("Cheque");
                            }else{
                                if(tipo == 2){
                                    m1.setTipo("Vale vista");
                                }else{
                                    if(tipo == 3){
                                        m1.setTipo("Efectivo");
                                    }
                                }
                            }
                            
                            if(!m1.cobro()){
                                System.out.println("");
                                System.out.println("Probablemente surgio un error");
                            }
                            
                            break;
                        
                        case 3:
                            //VER HISTORIAL CUENTA
                            
                            //Creacion del objeto h de la clase historial
                            //Permite consultar el historial de la cuenta que enviamos
                            //En este caso, la del indice seleccionado por el usuario
                            Historial h = new Historial();
                            h.mostrarHistorial(rescatadas.get(indiceCuenta));
                            
                            break;
                            
                        default:
                                    
                    }
                    
                    break;

                case 2:
                    
                    //AGREGAR CUENTA
                    //El enunciado solicita administrar 4 cuentas, como mínimo, por lo que
                    //se tiene la opcion de agregar nuevas cuentas
                    Cuenta c = new Cuenta();
                    System.out.println("** Agregar Cuenta **");
                    System.out.println("Ingrese numero de cuenta");
                    c.setNumCuenta(sc.nextInt());
                    System.out.println("Ingrese saldo");
                    c.setSaldo(sc.nextInt());
                    System.out.println("Ingrese nombre cuenta");
                    c.setNombre(scS.nextLine());
                    
                    //guardarCuenta añade los datos de la cuenta al archivo "Cuentas.txt"
                    //en el siguiente formato: numerocuenta/saldo/nombre
                    if(s.guardarCuenta(c)){
                        System.out.println("Cuenta agregada correctamente");
                    }else{
                        System.out.println("Ha surgido un problema");
                    }
                    
                    break;

                case 3:
                    System.out.println("");
                    System.out.println("Saliendo del sistema");
                    System.out.println("");
                    continuar = false;
                    break;
                default:
                    System.out.println("");
                    System.out.println("Ha ingresado una opcion que no corresponde");
                    System.out.println("¿Desea continuar?");
                    System.out.println("");
                    System.out.println("1. Si");
                    System.out.println("2. No");
                    opcion = sc.nextInt();
                    if(opcion == 1){
                        continuar = true;
                    }else{
                        continuar = false;
                    }
                    break;
            }
        }
    }   
    
    public ArrayList<Cuenta> rescatarCuentas() throws IOException{
        FileReader fr =null;
        BufferedReader bf = null;
        String linea;
        ArrayList<Cuenta> rescate = new ArrayList<>();
        String[] aux = new String[3];
        
        try{
            // Apertura del fichero y creacion de BufferedReader quien almacena
            // La linea que se va leyendo dentro del fichero mediante el metodo
            // readLine().
            fr = new FileReader ("Cuentas.txt");
            bf = new BufferedReader(fr);

            //Lectura del fichero
            
            while ((linea = bf.readLine())!=null) {
                aux = linea.split("/");
                Cuenta auxC = new Cuenta();
                auxC.setNumCuenta(Integer.parseInt(aux[0]));
                auxC.setSaldo(Integer.parseInt(aux[1]));
                auxC.setNombre(aux[2]);
                rescate.add(auxC);
            }
            
            fr.close();
            return rescate;
            
        }
        catch(Exception e){
            if(null != fr){
                fr.close();
            }
            return null;
        }
    }
    
    public boolean guardarCuenta(Cuenta c) throws IOException{
        FileWriter fichero = null;
        PrintWriter pw ;
        boolean bandera;
        try{
            fichero = new FileWriter("Cuentas.txt", true);
            pw = new PrintWriter(fichero);

            pw.println(c.getNumCuenta()+"/"+c.getSaldo()+"/"+c.getNombre());
            bandera = true;
            if(crearHistorialCuenta(c)){
                bandera = true;
            }else{
                bandera = false;
            }
            fichero.close();
            return bandera;
        }catch (Exception e) {
            if(fichero != null){
                fichero.close();
            }
            return false;
        }
    }   
    
    public boolean crearHistorialCuenta(Cuenta c) throws IOException{
        String ruta = c.getNombre()+".txt";
        File archivo = new File(ruta);
        BufferedWriter bw;
        if(archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
        }
        bw.close();
        
        return true;
    }
}