package stacarolina;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Historial {
    private ArrayList<Movimiento> movimientos;
    
    Historial(){
        movimientos = new ArrayList <>();
    }
    
    void mostrarHistorial(Cuenta c) throws IOException{
        
        FileReader fr =null;
        BufferedReader bf = null;
        String linea;
        ArrayList rescate = new ArrayList<>();
        String[] aux = new String[7];
        boolean estado ;
        
        try{
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            fr = new FileReader (c.getNombre()+".txt");
            bf = new BufferedReader(fr);

            //Lectura del fichero
            System.out.println("** HISTORIAL **");
            System.out.println("");
            while ((linea = bf.readLine())!=null) {
                //0"+"#"+monto+"#"+cliente.getNombre()+"#"+cliente.getApellido()+"#"+fecha+"#"+motivo+"#"+tipo
                aux = linea.split("#");
                if(aux[0].equals("0")){
                    estado = true;
                }else{
                    estado = false;
                }
                    
                Movimiento maux = new Movimiento();
                Cliente caux = new Cliente();
                caux.setNombre(aux[2]);
                caux.setApellido(aux[3]);
                
                maux.setMonto(Integer.parseInt(aux[1]));
                maux.setCliente(caux);
                maux.setFecha(aux[4]);
                maux.setMotivo(aux[5]);
                maux.setTipo(aux[6]);
                
                maux.datosMovimiento(estado);          
            }
            System.out.println("");
            System.out.println("SU SALDO CONTABLE ES: "+c.getSaldo());
            System.out.println("");
            System.out.println("** FIN MOVIMIENTOS **");
            fr.close();
                        
        }
        catch(Exception e){
            if(null != fr){
                fr.close();
            }
        }
    }
}
