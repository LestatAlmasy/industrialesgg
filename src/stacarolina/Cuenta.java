package stacarolina;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Cuenta {
    private int saldo;
    private int numCuenta;
    private String nombre;
   
    
    Cuenta(){
        
    }
    
    public boolean Ingreso(int monto) throws IOException{
        
        this.saldo = this.saldo + monto;
        actualizarSaldo();
        return true;
    }
    
    public boolean Egreso(int monto) throws IOException{
        if(monto > this.saldo){
            System.out.println("No hay saldo en la cuenta");
            return false;
        }else{
            this.saldo = this.saldo - monto;
            actualizarSaldo();
            return true;
        }
    }
    
    public void actualizarSaldo() throws IOException{
        String nomTxt = "Cuentas.txt";
        FileReader fr =null;
        BufferedReader bf = null;
        String linea;
        boolean borrar;
        ArrayList<Cuenta> rescate = new ArrayList<>();
        String[] aux = new String[3];
        
        
        
        try{
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            fr = new FileReader (nomTxt);
            bf = new BufferedReader(fr);

            //Lectura del fichero
            
            while ((linea = bf.readLine())!=null) {
                aux = linea.split("/");
                Cuenta auxC = new Cuenta();
                auxC.setNumCuenta(Integer.parseInt(aux[0]));
                auxC.setSaldo(Integer.parseInt(aux[1]));
                auxC.setNombre(aux[2]);
                rescate.add(auxC);
                
            }
            for(int i=0; i<rescate.size();i++){                
                if(this.nombre.equals(rescate.get(i).getNombre())) {
                    rescate.get(i).setSaldo(this.saldo);
                    BufferedWriter bw = new BufferedWriter(new FileWriter(nomTxt));
                    borrar = true;
                }
            }
            for(int i=0; i<rescate.size();i++){
                StaCarolina st = new StaCarolina();
                if(st.guardarCuenta(rescate.get(i))){
                    
                }else{
                    System.out.println("Ha ocurrido un error actualizando saldo");
                }
            }
            
            
            fr.close();            
        }
        catch(Exception e){
            if(null != fr){
                fr.close();
            }
        }
        
        this.saldo = saldo;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
