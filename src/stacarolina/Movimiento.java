package stacarolina;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Movimiento {
    private Cliente cliente;
    private String fecha;
    private int monto;
    private String motivo;
    private Cuenta cuenta;
    //cheque, vale vista, efectivo
    private String tipo;
    
    Movimiento(){
        
    }
    
    public boolean pago () throws IOException{
        PrintWriter pw ;
        
        if(cuenta.Ingreso(this.monto)){
            System.out.println("Usted a pagado "+this.monto+" a la cuenta n° "+cuenta.getNumCuenta());
            FileWriter fichero = null;
        
            try{
                fichero = new FileWriter(cuenta.getNombre()+".txt", true);
                pw = new PrintWriter(fichero);
                //0 sera pago
                pw.println("0"+"#"+monto+"#"+cliente.getNombre()+"#"+cliente.getApellido()+"#"+fecha+"#"+motivo+"#"+tipo);
                fichero.close();
            }catch (Exception e) {
                if(fichero != null){
                    fichero.close();
                }
                return false;
            }
            return true;
        }else{
            System.out.println("No se ha podido realizar el movimiento");
            return false;
        }
    }
    
    public boolean cobro() throws IOException{
        System.out.println("me estoy cobrando");
        PrintWriter pw ;
        if(cuenta.Egreso(this.monto)){
            System.out.println("Usted a cobrado "+this.monto+" desde la cuenta n° "+cuenta.getNumCuenta());
            FileWriter fichero = null;
        
            try{
                fichero = new FileWriter(cuenta.getNombre()+".txt", true);
                pw = new PrintWriter(fichero);
                //1 sera cobro
                pw.println("1"+"#"+monto+"#"+cliente.getNombre()+"#"+cliente.getApellido()+"#"+fecha+"#"+motivo+"#"+tipo);
                fichero.close();
            }catch (Exception e) {
                if(fichero != null){
                    fichero.close();
                }
                return false;
            }
            return true;
        }else{
            System.out.println("No se ha podido realizar el movimiento");
            return false;
        }
        
    }

    public void datosMovimiento(boolean estado){
        if(estado){
            //Pago
            System.out.println("**********************************************************");
            System.out.println("+ "+this.monto+" | Efectuado por: "+ cliente.getNombre()+ " "+ cliente.getApellido()+ " | fecha: "+ fecha +" | razón de: "+ motivo+" | medio:"+tipo);
            System.out.println("**********************************************************");
        }else{
            //Cobro
            System.out.println("**********************************************************");
            System.out.println("- "+this.monto+" | Efectuado por: "+ cliente.getNombre()+ " "+ cliente.getApellido()+ " con fecha: "+ fecha +" a razón de: "+ motivo+" mediante un:"+tipo);
            System.out.println("**********************************************************");
        }
    }
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
}
